import java.io.File;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created with IntelliJ IDEA.
 * User: Chernyshov Yuriy
 * Date: 18.05.13
 * Time: 11:17
 */
public class Main {

    private AtomicBoolean isProducerDone = new AtomicBoolean();

    public static void main(String[] args) {
        if (args != null) {
            if (args.length >= 2) {
                String rootDirPath = args[0];
                int depthLevel = Integer.valueOf(args[1]);
                String searchPattern = null;
                if (args.length == 3) {
                    searchPattern = args[2];
                }

                Main main = new Main();

                final int bufferSize = 100;
                final LinkedList<String> mainQueue = new LinkedList<String>();

                Thread producerThread = new Thread(main.new Producer(rootDirPath, depthLevel, searchPattern, mainQueue,
                        bufferSize));
                Thread consumerThread = new Thread(main.new Consumer(mainQueue));

                producerThread.setName("Producer Thread");
                consumerThread.setName("Consumer Thread");

                producerThread.start();
                consumerThread.start();

            } else {
                printError("Not all arguments specified");
            }
        } else {
            printError("No arguments specified");
        }
    }

    public Main() {

    }

    private class Producer implements Runnable {

        private String mRootDirPath;
        private int mDepthLevel;
        private final int BUFFER_SIZE;
        private final LinkedList<String> mMainQueue;
        private String mSearchPattern = null;

        private Producer(String rootDirPath, int depthLevel, String searchPattern, LinkedList<String> mainQueue,
                         int bufferSize) {
            mRootDirPath = rootDirPath;
            mDepthLevel = depthLevel;
            mSearchPattern = searchPattern;
            BUFFER_SIZE = bufferSize;
            mMainQueue = mainQueue;
            isProducerDone.set(Boolean.FALSE);
        }

        private void produce(String value) throws InterruptedException {
            //wait if queue is full
            while (mMainQueue.size() == BUFFER_SIZE) {
                synchronized (mMainQueue) {
                    System.out.println("Queue is full '" + Thread.currentThread().getName()
                            + "' is waiting, size: " + mMainQueue.size());
                    mMainQueue.wait();
                }
            }
            //producing element and notify consumers
            synchronized (mMainQueue) {
                printMessage("Producing: " + value);
                mMainQueue.push(value);
                mMainQueue.notifyAll();
            }
        }

        @Override
        public void run() {
            LinkedList<FileItem> queue = new LinkedList<FileItem>();
            FileItem fileItem = new FileItem();
            fileItem.setValue(new File(mRootDirPath));
            fileItem.setDepth(0);
            queue.add(fileItem);
            int currentDepth;
            while (!queue.isEmpty()) {
                fileItem = queue.remove();
                currentDepth = fileItem.getDepth() + 1;
                if (mDepthLevel != -1 && currentDepth == mDepthLevel) {
                    continue;
                }
                File[] files = fileItem.getValue().listFiles();
                if (files != null) {
                    for (File child : files) {
                        if (mSearchPattern != null && child.getName().contains(mSearchPattern)) {
                            try {
                                produce(child.getName());
                            } catch (InterruptedException e) {
                                printError("Producer error: " + e.toString());
                            }
                        }
                        fileItem = new FileItem();
                        fileItem.setValue(child);
                        fileItem.setDepth(currentDepth);
                        if (fileItem.getValue().isDirectory()) {
                            queue.add(fileItem);
                        }
                    }
                }
            }
            printMessage("Shut down producer");
            isProducerDone.set(Boolean.TRUE);
            synchronized (mMainQueue) {
                mMainQueue.notifyAll();
            }
        }
    }

    private class Consumer implements Runnable {

        private final LinkedList<String> mMainQueue;

        private Consumer(LinkedList<String> mainQueue) {
            mMainQueue = mainQueue;
        }

        public void consume() throws InterruptedException {
            //wait if queue is empty
            while (mMainQueue.isEmpty() && !isProducerDone.get()) {
                synchronized (mMainQueue) {
                    System.out.println("Queue is empty '" + Thread.currentThread().getName()
                            + "' is waiting, size: " + mMainQueue.size());
                    mMainQueue.wait();
                }
            }
            //Otherwise consume element and notify waiting producer
            synchronized (mMainQueue) {
                mMainQueue.notifyAll();
                if (!mMainQueue.isEmpty()) {
                    printMessage("Consuming: " + mMainQueue.remove());
                }
            }
        }

        @Override
        public void run() {
            while (!isProducerDone.get()) {
                synchronized (mMainQueue) {
                    try {
                        consume();
                    } catch (InterruptedException e) {
                        printError("Consumer error: " + e.toString());
                    }
                }
            }
            printMessage("Shut down consumer");
        }
    }

    private static void printMessage(String message) {
        System.out.println("[I] " + message);
    }

    private static void printError(String message) {
        System.out.println("[E] " + message);
    }
}