import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: Chernyshov Yuriy
 * Date: 18.05.13
 * Time: 11:17
 */
public class FileItem {

    private int depth;
    private File value;

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public File getValue() {
        return value;
    }

    public void setValue(File value) {
        this.value = value;
    }
}